import argparse

import boto3
from dotenv import dotenv_values
from simplemysql import SimpleMysql

import helper

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to do optimization simulation test')
    parser.add_argument("-i", "--input-file", metavar='INPUT_FILE', help='input file containing VIDEO_VERSION_SERIAL')
    parser.add_argument('-o', '--output-file', metavar='OUTPUT_FILE', help='output file containing path of all downloaded videos')

    args = parser.parse_args()

    # parse config

    video_db_conf = dotenv_values("video_db.env")

    s3_conf = dotenv_values("s3.env")

    app_conf = dotenv_values("app.env")

    # init botos3
    print(s3_conf['AWS_KEY'])
    print(s3_conf['AWS_SECRET'])
    s3_client = boto3.client(
        's3',
        aws_access_key_id=s3_conf['AWS_KEY'],
        aws_secret_access_key=s3_conf['AWS_SECRET'],
        region_name='ap-southeast-1'
    )

    # init connection to mysql

    print("init db")
    print("vars {}", print(video_db_conf))
    video_db = SimpleMysql(
        host=video_db_conf['HOST'],
        db=video_db_conf['DB'],
        user=video_db_conf['USER'],
        passwd=video_db_conf['PASSWORD'],
        keep_alive=True,
        charset='utf8mb4'
    )
    print("success init db")

    downloaded_file_list = []

    with open(args.input_file, 'r') as f:
        while True:

            line = f.readline()

            if not line:
                break

            video_version_serial = line.strip()

            print('Processing video_version_serial {}'.format(video_version_serial))

            # get video_version detail
            vv = helper.get_video_detail(video_db, video_version_serial)

            print("Now downloading from original_file_path {}".format(vv.original_file_path))

            result_path = helper.download_source_by_file_path(app_conf, s3_conf, s3_client, vv.original_file_path)

            print("Downloaded to {}".format(result_path))

            downloaded_file_list.append(result_path)

    with open(args.output_file, 'w') as out_f:
        out_f.write('\n'.join(downloaded_file_list))
