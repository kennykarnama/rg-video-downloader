# rg-video-downloader

Script to download original videos based on video_version_serial

# Usage

```shell
usage: main.py [-h] [-i INPUT_FILE] [-o OUTPUT_FILE]

Script to do optimization simulation test

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input-file INPUT_FILE
                        input file containing VIDEO_VERSION_SERIAL
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        output file containing path of all downloaded videos
```

## input file

newline separated rgvv code

example

```text
RGVV-XXX
RGVV-YYY
```

## output file

All downloaded files will be output to a new txt file

```text
[a folder]/[a file].mp4
```

## envar

Please provide the following `.env` file

### video_db.env

```text
HOST=
DB="rg-video"
USER=
PASSWORD=
```

### app.env

```text
DOWNLOAD_OUTPUT_DIR=[your out dir]
```

make sure to create the folder first

### s3.env

```text
LOOKUP_BUCKET=rubel-video-input
AWS_KEY=
AWS_SECRET=
```