import os

from s3urls import parse_url

import util


def download_source_by_file_path(app_conf, s3_conf, botoS3Client, original_file_path):
    result_path = None
    if parsed := parse_as_s3(original_file_path) is not None:
        # download using s3 with the bucket specified inside the original_file_path
        result_path = download_from_S3(app_conf, botoS3Client, parsed['bucket'], parsed['key'])
    else:
        # download by assuming its samples equals to what has been set in s3_conf
        result_path = download_from_S3(app_conf, botoS3Client, s3_conf['LOOKUP_BUCKET'], original_file_path)
    return result_path


def parse_as_s3(original_file_path):
    parsed = None
    try:
        parsed = parse_url(original_file_path)
    finally:
        return parsed


# download_from_s3
# accepts botoS3Client
# bucket and key
# by default, it will write
# the file to /tmp/[key]
def download_from_S3(app_conf, botoS3Client, bucket, key):
    fname = app_conf["DOWNLOAD_OUTPUT_DIR"] + "/" + util.path_leaf(key)

    # if exist, skip downloading
    if os.path.exists(fname):
        print("{} already exist".format(fname))
        return fname

    botoS3Client.download_file(bucket, key, fname)

    return fname


# get_video_detail
# accepts parameter
# video_db (conn object to mysql)
# video_version_serial
# returns a Video Object
def get_video_detail(video_db, video_version_serial):
    vv = video_db.getOne("video_version", ['original_file_path'], ("serial = %s", [video_version_serial]))
    return vv